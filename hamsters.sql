create DATABASE hamsters;
use hamsters;
CREATE TABLE Dono(
    id_dono INT UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(50),
    endereco VARCHAR(50), 
	email VARCHAR(50) unique ,
	senha VARCHAR(20)
    );

CREATE TABLE Veterinario(
    id_veterinario INT UNIQUE NOT NULL  AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(50),
    descricao VARCHAR(30),
    contato VARCHAR(30)
);
CREATE TABLE Hamster(
    id_hamster INT UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(20),
    id_dono INT,
    FOREIGN KEY (id_dono) REFERENCES Dono(id_dono)
);
CREATE TABLE Vacina(
    id_vacina INT UNIQUE NOT NULL AUTO_INCREMENT PRIMARY KEY,
    descricao VARCHAR(50),
    data VARCHAR(10),
    local VARCHAR(30),
    id_hamster INT,
    id_veterinario INT,
    FOREIGN KEY (id_hamster) REFERENCES Hamster(id_hamster),
    FOREIGN KEY (id_veterinario) REFERENCES Veterinario(id_veterinario)
);



INSERT INTO Dono VALUES(null, "Luisa", "Av. Italia, 145","email@email.com", "senha");
INSERT INTO Dono VALUES(null, "Roberto", "Av. Arábia, 122","email@","senha");
INSERT INTO Dono VALUES(null, "Alessandro", "Av. Brasil, 249","email@email","senha");

INSERT INTO Veterinario VALUES(null, "Leandro", "Dr. Leandro BIRL", "(51) 9 8041-1191");
INSERT INTO Veterinario VALUES(null, "Lucas", "Dr. Lucas Batista", "(51) 9 4203-3925");
INSERT INTO Veterinario VALUES(null, "Luana", "Dra. Luana da Rosa", "(51) 9 9002-8422");


INSERT INTO Hamster VALUES (null, "Stuart Little", (SELECT id_dono FROM Dono WHERE nome = "Roberto"));
INSERT INTO Hamster VALUES (null, "Ratatuille", (SELECT id_dono FROM Dono WHERE nome = "Luisa"));
INSERT INTO Hamster VALUES (null, "Wall-e", (SELECT id_dono FROM Dono WHERE nome = "Alessandro"));
INSERT INTO Hamster VALUES (null, "Wall-e", (SELECT id_dono FROM Dono WHERE nome = "Ritinha"));
INSERT INTO Hamster VALUES (null, "Elliot", (SELECT id_dono FROM Dono WHERE nome = "Ritinha"));
INSERT INTO Hamster VALUES (null, "Jones", (SELECT id_dono FROM Dono WHERE nome = "Ritinha"));
INSERT INTO Hamster VALUES (null, "Joianes", (SELECT id_dono FROM Dono WHERE nome = "Ritinha"));



delete from hamster where id_dono = 3;
INSERT INTO Vacina VALUES(NULL, "Antirrábica", NOW(), "Clinica do Dr. Lucas", (SELECT id_hamster FROM Hamster WHERE nome = "Stuart Little"), 
(SELECT id_veterinario FROM Veterinario WHERE nome = "Lucas"));

SELECT * FROM Hamster;
SELECT * FROM Vacina;
SELECT * FROM Dono;
SELECT * FROM Veterinario;