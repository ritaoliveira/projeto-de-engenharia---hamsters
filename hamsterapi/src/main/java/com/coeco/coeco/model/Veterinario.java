package com.coeco.coeco.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="veterinario")
public class Veterinario {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_veterinario")
	private long idVeterinario;
	@Column(name="nome")
	private String nome;
	
	@Column(name="descricao")
	private String descricao;

	@Column(name="contato")
	private String contato;

	public long getIdVeterinario() {
		return idVeterinario;
	}

	public void setIdVeterinario(long idVeterinario) {
		this.idVeterinario = idVeterinario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	@Override
	public String toString() {
		return "Veterinario: " + nome + ", descricao: " + descricao + ", contato: " + contato;
	}
	

}
