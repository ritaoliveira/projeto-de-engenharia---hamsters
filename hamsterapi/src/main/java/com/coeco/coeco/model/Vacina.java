package com.coeco.coeco.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="vacina")
public class Vacina {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_vacina")
	private long idVacina;
		
	@Column(name="descricao")
	private String descricao;
	
	@Column(name="data")
	private String data;
	
	@Column(name="local")
	private String local;
	
	@ManyToOne
	@JoinColumn(name = "id_veterinario", nullable = false) 
	private Veterinario veterinario;
	
	@ManyToOne
	@JoinColumn(name = "id_hamster", nullable = false) 
	private Hamster hamster;

	public long getIdVacina() {
		return idVacina;
	}

	public void setIdVacina(long idVacina) {
		this.idVacina = idVacina;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public Veterinario getVeterinario() {
		return veterinario;
	}

	public void setVeterinario(Veterinario veterinario) {
		this.veterinario = veterinario;
	}

	public Hamster getHamster() {
		return hamster;
	}

	public void setHamster(Hamster hamster) {
		this.hamster = hamster;
	}

	@Override
	public String toString() {
		return "Vacina: " + descricao + ", data: " + data + ", realizada em: " + local + ", realizada por: "+veterinario
				+ ", em: " + hamster;
	}
	
}
