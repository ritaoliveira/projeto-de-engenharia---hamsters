package com.coeco.coeco.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="hamster")
public class Hamster {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_hamster")
	private long idHamster;
	@Column(name="nome")
	private String nome;
	@ManyToOne
	@JoinColumn(name = "id_dono", nullable = false) 
	private Dono dono;
	public long getIdHamster() {
		return idHamster;
	}
	public void setIdHamster(long idHamster) {
		this.idHamster = idHamster;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Dono getDono() {
		return dono;
	}
	public void setDono(Dono dono) {
		this.dono = dono;
	}
	@Override
	public String toString() {
		return "Hamster: " + nome + ", dono: " + dono;
	}
	
	
}
