package com.coeco.coeco.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.coeco.coeco.model.Dono;
import com.coeco.coeco.model.Hamster;
import com.coeco.coeco.repository.DonoRepository;
import com.coeco.coeco.repository.HamsterRepository;

@RestController
@RequestMapping(path = "/hamster")
public class HamsterController {
	@Autowired
	private HamsterRepository repo;
	@Autowired
	private DonoRepository repoDono;

	@GetMapping(path = "/all")
	public ResponseEntity<List<Hamster>> getAllVacinas() { 
		return ResponseEntity.ok(repo.findAll());
	}
	@GetMapping(path = "/idDono/{idDono}")
	public ResponseEntity<List<Hamster>> getAllHamsterByDono(@PathVariable(required = true, name = "idDono") long idUsuario) { 
		Dono dono = repoDono.findByIdUsuario(idUsuario);
		return ResponseEntity.ok(repo.findHamsterByDono(dono));
	}
	@GetMapping(path = "/nome/{nome}")
	public ResponseEntity<Hamster> getHamsterByNome(@PathVariable(required = true, name = "nome") String nome) { 
		return ResponseEntity.ok(repo.findHamsterByNome(nome));
	}
	@PostMapping
	public ResponseEntity<Hamster>addHamsterPost(@RequestBody Hamster hamster) {
		return ResponseEntity.ok(repo.save(hamster));
	}
	@PutMapping("/{idHamster}")
	public ResponseEntity<Hamster> updateHamster(@PathVariable long idHamster, @RequestBody Hamster hamster){
		repo.save(hamster);
		return new ResponseEntity<Hamster>(hamster, HttpStatus.OK);
}
	@DeleteMapping(path = "/idHamster/{idHamster}")
	@ResponseStatus(code=HttpStatus.OK)
	public void deleteHamsterById(@PathVariable(required=true, name="idHamster") long idHamster) {
	repo.deleteById(idHamster);
}

}
