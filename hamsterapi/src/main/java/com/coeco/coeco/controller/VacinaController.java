package com.coeco.coeco.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.coeco.coeco.model.Dono;
import com.coeco.coeco.model.Hamster;
import com.coeco.coeco.model.Vacina;
import com.coeco.coeco.repository.DonoRepository;
import com.coeco.coeco.repository.VacinaRepository;

@RestController
@RequestMapping(path = "/vacina")
public class VacinaController {
	@Autowired
	private VacinaRepository repo;

	@GetMapping(path = "/all")
	public ResponseEntity<List<Vacina>> getAllVacinas() { 
		return ResponseEntity.ok(repo.findAll());
	}
	@GetMapping(path = "/idHamster/{idHamster}")
	public ResponseEntity<List<Vacina>> getAllVacinasByHamster(@PathVariable(required = true, name = "idHamster") long idHamster) { 
		return ResponseEntity.ok(repo.findAll());
	}
	@PostMapping
	public ResponseEntity<Vacina >addVacinaPost(@RequestBody Vacina vacina) {
		return ResponseEntity.ok(repo.save(vacina));
	}
	@PutMapping("/idVacina/{idVacina}")
	public ResponseEntity<Vacina> updateVacina(@PathVariable("idVacina") long idVacina, @RequestBody Vacina vacina){
		repo.save(vacina);
		return new ResponseEntity<Vacina>(vacina, HttpStatus.OK);
}
	@DeleteMapping(path = "/idVacina/{idVacina}")
	@ResponseStatus(code=HttpStatus.OK)
	public void deleteVacinaById(@PathVariable(required=true, name="idVacina") long idVacina) {
	repo.deleteById(idVacina);
	}
}
