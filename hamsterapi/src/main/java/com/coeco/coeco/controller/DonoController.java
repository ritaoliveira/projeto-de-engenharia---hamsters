package com.coeco.coeco.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.coeco.coeco.model.Dono;
import com.coeco.coeco.model.Hamster;
import com.coeco.coeco.repository.DonoRepository;
import com.coeco.coeco.service.LoginService;

@RestController
@RequestMapping(path = "/dono")
public class DonoController {
	@Autowired
	private DonoRepository repo;

	@GetMapping(path = "/all")
	public ResponseEntity<List<Dono>> getAllUsers() { 
		return ResponseEntity.ok(repo.findAll());
	}
	@GetMapping("/email/{email}")
	public Dono getUserByEmail(@PathVariable String email) {
		return repo.findByEmail(email);
	}
	
	@PostMapping
	public ResponseEntity<Dono>addUsuarioPost(@RequestBody Dono dono) {
		return ResponseEntity.ok(repo.save(dono));
	}
	@PutMapping("/{idDono}")
	public ResponseEntity<Dono> updateHamster(@PathVariable long idDono, @RequestBody Dono dono){
		repo.save(dono);
		return new ResponseEntity<Dono>(dono, HttpStatus.OK);
}
	@DeleteMapping(path = "/idDono/{idDono}")
	@ResponseStatus(code=HttpStatus.OK)
	public void deleteUserById(@PathVariable(required=true, name="idDono") long idDono) {
	repo.deleteById(idDono);

	
	}
}
