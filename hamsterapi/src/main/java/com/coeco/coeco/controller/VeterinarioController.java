package com.coeco.coeco.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.coeco.coeco.model.Hamster;
import com.coeco.coeco.model.Vacina;
import com.coeco.coeco.model.Veterinario;
import com.coeco.coeco.repository.VeterinarioRepository;

@RestController
@RequestMapping(path = "/veterinario")
public class VeterinarioController {
	@Autowired
	private VeterinarioRepository repo;

	@GetMapping(path = "/all")
	public ResponseEntity<List<Veterinario>> getAllVeterinarios() { 
		return ResponseEntity.ok(repo.findAll());
	}

	@PostMapping
	public ResponseEntity<Veterinario>addVeterinarioPost(@RequestBody Veterinario veterinario) {
		return ResponseEntity.ok(repo.save(veterinario));
	}
	@PutMapping("/idVeterinario/{idVeterinario}")
	public ResponseEntity<Veterinario> updateVeterinario(@PathVariable("idVeterinario") long idVeterinario, @RequestBody Veterinario veterinario){
		repo.save(veterinario);
		return new ResponseEntity<Veterinario>(veterinario, HttpStatus.OK);
}
	@GetMapping(path = "/nome/{nome}")
	public ResponseEntity<Veterinario> getVetByNome(@PathVariable(required = true, name = "nome") String nome) { 
		return ResponseEntity.ok(repo.findVetByNome(nome));
	}
	@GetMapping(path = "/idVeterinario/{idVeterinario}")
	public ResponseEntity<Veterinario> getVetById(@PathVariable(required = true, name = "idVeterinario") long idVeterinario) { 
		return ResponseEntity.ok(repo.findVetByIdVeterinario(idVeterinario));
	}
	@DeleteMapping(path = "/idVeterinario/{idVeterinario}")
	@ResponseStatus(code=HttpStatus.OK)
	public void deleteVeterinarioById(@PathVariable(required=true, name="idVeterinario") long idVeterinario) {
	repo.deleteById(idVeterinario);
	}
}
