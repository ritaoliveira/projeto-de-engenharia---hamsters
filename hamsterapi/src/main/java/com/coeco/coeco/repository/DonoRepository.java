package com.coeco.coeco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coeco.coeco.model.Dono;

public interface DonoRepository extends JpaRepository<Dono, Long>{
	Dono findByIdUsuario (long idUsuario);
	Dono findByEmail(String email);
	Dono findBySenha(String senha);

}
