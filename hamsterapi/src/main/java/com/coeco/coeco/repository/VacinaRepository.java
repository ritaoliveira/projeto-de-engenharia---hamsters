package com.coeco.coeco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coeco.coeco.model.Vacina;

public interface VacinaRepository extends JpaRepository <Vacina, Long>{

}
