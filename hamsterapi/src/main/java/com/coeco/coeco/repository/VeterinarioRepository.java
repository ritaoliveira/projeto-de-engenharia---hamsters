package com.coeco.coeco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coeco.coeco.model.Veterinario;

public interface VeterinarioRepository extends JpaRepository<Veterinario, Long>{

	Veterinario findVetByNome(String nome);

	Veterinario findVetByIdVeterinario(long idVeterinario);

}
