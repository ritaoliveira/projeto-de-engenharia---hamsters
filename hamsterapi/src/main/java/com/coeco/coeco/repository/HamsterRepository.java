package com.coeco.coeco.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coeco.coeco.model.Dono;
import com.coeco.coeco.model.Hamster;

public interface HamsterRepository extends JpaRepository<Hamster, Long> {

	 List<Hamster >findHamsterByDono(Dono dono);

	Hamster findHamsterByNome(String nome);

}
