-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: tcc_coeco
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `avaliacao`
--

DROP TABLE IF EXISTS `avaliacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `avaliacao` (
  `id_avaliacao` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuario` bigint(20) DEFAULT NULL,
  `id_ponto` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_avaliacao`),
  UNIQUE KEY `id_avaliacao` (`id_avaliacao`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_ponto` (`id_ponto`),
  CONSTRAINT `avaliacao_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `avaliacao_ibfk_2` FOREIGN KEY (`id_ponto`) REFERENCES `ponto` (`id_ponto`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacao`
--

LOCK TABLES `avaliacao` WRITE;
/*!40000 ALTER TABLE `avaliacao` DISABLE KEYS */;
INSERT INTO `avaliacao` VALUES (2,3,10),(3,2,10),(4,2,10),(5,2,10),(6,12,10),(7,12,10);
/*!40000 ALTER TABLE `avaliacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classe`
--

DROP TABLE IF EXISTS `classe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classe` (
  `id_classe` int(11) NOT NULL AUTO_INCREMENT,
  `classe` varchar(100) NOT NULL,
  PRIMARY KEY (`id_classe`),
  UNIQUE KEY `id_classe` (`id_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classe`
--

LOCK TABLES `classe` WRITE;
/*!40000 ALTER TABLE `classe` DISABLE KEYS */;
INSERT INTO `classe` VALUES (1,'Pilhas e Baterias'),(2,'Lâmpadas'),(3,'Vidro'),(4,'Vestuário'),(5,'Embalagens longa vida'),(6,'Eletrônicos'),(7,'Plástico'),(8,'Papel e papelão'),(9,'Metais'),(10,'Eletrodomésticos'),(11,'Materiais orgânicos'),(12,'Óleos'),(13,'Líquidos e produtos químicos'),(14,'Veículos'),(15,'Diversos'),(16,'Móveis'),(17,'Construção e demolição'),(18,'Remédios');
/*!40000 ALTER TABLE `classe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `endereco`
--

DROP TABLE IF EXISTS `endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `endereco` (
  `id_endereco` bigint(20) NOT NULL AUTO_INCREMENT,
  `uf` varchar(2) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `cep` varchar(8) DEFAULT NULL,
  UNIQUE KEY `id_endereco` (`id_endereco`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `endereco`
--

LOCK TABLES `endereco` WRITE;
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
INSERT INTO `endereco` VALUES (1,'RS','São Leopoldo','Arthur Berger',115,'930370'),(2,'RJ','Petrópolis','Rua Manoel Silva',138,'57547'),(3,'PA','Salvador','Rua João',3000,'123456'),(4,'DF','Brasília','Bolsonaro',17000,'1717171');
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (6);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponto`
--

DROP TABLE IF EXISTS `ponto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponto` (
  `id_ponto` bigint(20) NOT NULL AUTO_INCREMENT,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `id_usuario` bigint(20) DEFAULT NULL,
  `id_endereco` bigint(20) DEFAULT NULL,
  `tipo` varchar(15) DEFAULT NULL,
  UNIQUE KEY `id_ponto` (`id_ponto`),
  KEY `id_usuario` (`id_usuario`),
  KEY `id_endereco` (`id_endereco`),
  CONSTRAINT `ponto_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `ponto_ibfk_2` FOREIGN KEY (`id_endereco`) REFERENCES `endereco` (`id_endereco`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponto`
--

LOCK TABLES `ponto` WRITE;
/*!40000 ALTER TABLE `ponto` DISABLE KEYS */;
INSERT INTO `ponto` VALUES (1,4,5,NULL,NULL,'ENTREGA'),(9,77,77,2,NULL,NULL),(10,4.5,4.4,2,NULL,NULL),(11,-297602800,-511472200,2,NULL,'IRREGULAR'),(12,0,0,NULL,NULL,NULL),(13,0,0,NULL,NULL,NULL),(14,0,0,NULL,NULL,NULL),(15,0,0,NULL,NULL,NULL),(16,0,0,NULL,NULL,NULL),(17,0,0,NULL,NULL,NULL),(18,0,0,NULL,NULL,NULL),(19,0,0,NULL,NULL,NULL),(20,0,0,NULL,NULL,NULL),(21,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ponto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponto_coleta`
--

DROP TABLE IF EXISTS `ponto_coleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponto_coleta` (
  `disponibilidade` varchar(100) NOT NULL,
  `data_adicao` varchar(10) DEFAULT NULL,
  `av_qualidade` tinyint(4) DEFAULT NULL,
  `av_mobilidade` tinyint(4) DEFAULT NULL,
  `av_veracidade` tinyint(4) DEFAULT NULL,
  `id_ponto` bigint(20) DEFAULT NULL,
  KEY `id_ponto` (`id_ponto`),
  CONSTRAINT `ponto_coleta_ibfk_1` FOREIGN KEY (`id_ponto`) REFERENCES `ponto` (`id_ponto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponto_coleta`
--

LOCK TABLES `ponto_coleta` WRITE;
/*!40000 ALTER TABLE `ponto_coleta` DISABLE KEYS */;
INSERT INTO `ponto_coleta` VALUES ('14hrs',NULL,0,0,0,1);
/*!40000 ALTER TABLE `ponto_coleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponto_entrega`
--

DROP TABLE IF EXISTS `ponto_entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponto_entrega` (
  `contato` varchar(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `horario_fun` varchar(100) DEFAULT NULL,
  `duracao` varchar(16) DEFAULT NULL,
  `av_infra` tinyint(4) DEFAULT NULL,
  `av_mobilidade` tinyint(4) DEFAULT NULL,
  `av_organizacao` tinyint(4) DEFAULT NULL,
  `id_ponto` bigint(20) DEFAULT NULL,
  KEY `id_ponto` (`id_ponto`),
  CONSTRAINT `ponto_entrega_ibfk_1` FOREIGN KEY (`id_ponto`) REFERENCES `ponto` (`id_ponto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponto_entrega`
--

LOCK TABLES `ponto_entrega` WRITE;
/*!40000 ALTER TABLE `ponto_entrega` DISABLE KEYS */;
INSERT INTO `ponto_entrega` VALUES ('44464','Lixo',NULL,NULL,0,NULL,0,10),('','','null:  - ',' - ',0,NULL,0,12),('','','null:  - ',' - ',0,NULL,0,13),('','','null:  - ',' - ',0,NULL,0,14),('999999999','ponto massa','null: 12 - 12',' - ',0,NULL,0,15),('999999999','ponto massa','null: 12 - 12',' - ',0,NULL,0,16),('999999999','ponto massa','null: 12 - 12',' - ',0,NULL,0,17),('9999999','oi','null: 12 - 12',' - ',0,NULL,0,18),('6666666','ta','null: 13 - 13',' - ',0,NULL,0,20),('666','oi','null: 55 - 55',' - ',0,NULL,0,21);
/*!40000 ALTER TABLE `ponto_entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponto_irregular`
--

DROP TABLE IF EXISTS `ponto_irregular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponto_irregular` (
  `descricao` varchar(300) NOT NULL,
  `foto` varchar(200) DEFAULT NULL,
  `av_volume` tinyint(4) DEFAULT NULL,
  `av_periculosidade` tinyint(4) DEFAULT NULL,
  `id_ponto` bigint(20) DEFAULT NULL,
  KEY `id_ponto` (`id_ponto`),
  CONSTRAINT `ponto_irregular_ibfk_1` FOREIGN KEY (`id_ponto`) REFERENCES `ponto` (`id_ponto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponto_irregular`
--

LOCK TABLES `ponto_irregular` WRITE;
/*!40000 ALTER TABLE `ponto_irregular` DISABLE KEYS */;
INSERT INTO `ponto_irregular` VALUES ('Lixo no bueiro',NULL,0,0,11),('Canoas ',NULL,0,0,19);
/*!40000 ALTER TABLE `ponto_irregular` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `residuo`
--

DROP TABLE IF EXISTS `residuo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `residuo` (
  `id_residuo` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_subclasse` int(11) NOT NULL,
  `unidade` varchar(20) DEFAULT NULL,
  `quantidade` double DEFAULT NULL,
  `id_ponto` bigint(20) DEFAULT NULL,
  UNIQUE KEY `id_residuo` (`id_residuo`),
  KEY `id_ponto` (`id_ponto`),
  KEY `id_subclasse` (`id_subclasse`),
  CONSTRAINT `residuo_ibfk_1` FOREIGN KEY (`id_ponto`) REFERENCES `ponto` (`id_ponto`),
  CONSTRAINT `residuo_ibfk_2` FOREIGN KEY (`id_subclasse`) REFERENCES `subclasse` (`id_subclasse`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `residuo`
--

LOCK TABLES `residuo` WRITE;
/*!40000 ALTER TABLE `residuo` DISABLE KEYS */;
INSERT INTO `residuo` VALUES (4,1,'cm',5,1);
/*!40000 ALTER TABLE `residuo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subclasse`
--

DROP TABLE IF EXISTS `subclasse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subclasse` (
  `id_subclasse` int(11) NOT NULL AUTO_INCREMENT,
  `classe` varchar(100) NOT NULL,
  `id_classe` int(11) NOT NULL,
  PRIMARY KEY (`id_subclasse`),
  UNIQUE KEY `id_subclasse` (`id_subclasse`),
  KEY `id_classe` (`id_classe`),
  CONSTRAINT `subclasse_ibfk_1` FOREIGN KEY (`id_classe`) REFERENCES `classe` (`id_classe`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subclasse`
--

LOCK TABLES `subclasse` WRITE;
/*!40000 ALTER TABLE `subclasse` DISABLE KEYS */;
INSERT INTO `subclasse` VALUES (1,'Baterias de celular',1),(2,'Baterias de eletrônico',1),(3,'Baterias portáteis',1),(4,'Lâmpadas fluorescentes',2),(5,'Lâmpadas halógenas',2),(6,'Lâmpadas incadescentes',2),(7,'Chapas de vidro revestidas',3),(8,'Chapas de vidro simples',3),(9,'Espelhos',3),(10,'Garrafas de vidro',3),(11,'Vidros de janela',3),(12,'Calçados',4),(16,'Calçados esportivos',4),(17,'Roupas',4),(18,'Embalagens longa vida',5);
/*!40000 ALTER TABLE `subclasse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(80) NOT NULL,
  `email` varchar(200) NOT NULL,
  `senha` varchar(200) NOT NULL,
  `data_nasc` varchar(10) DEFAULT NULL,
  `contato1` varchar(11) NOT NULL,
  `contato2` varchar(11) DEFAULT NULL,
  `pt_adicionado` tinyint(4) DEFAULT NULL,
  `pt_confirmado` tinyint(4) DEFAULT NULL,
  `pt_entregue` tinyint(4) DEFAULT NULL,
  UNIQUE KEY `id_usuario` (`id_usuario`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Lucas','lucas@tt','3aa606ea44e2696362dcffae1eb7bd5e',NULL,'464464',NULL,0,0,0),(2,'tayse','t','e358efa489f58062f10dd7316b65649e',NULL,'t',NULL,0,0,0),(3,'Carolyn','carolyn2@','1234oi','2010-10-09','1234','1234',6,4,3),(4,'Rita','rita@','1234',NULL,'92290010',NULL,0,0,0),(5,'Mari','mari@','1234',NULL,'92290010',NULL,0,0,0),(7,'Joana','joana@','1234',NULL,'92290010',NULL,0,0,0),(8,'Mauricio','mauricio@','1234',NULL,'1234',NULL,0,0,0),(9,'Nereu','nereu@','1234','2010-10-09','1234',NULL,0,0,0),(10,'Vinilda','vinilda@','1234','2010-10-09','1234','1234',1,1,1),(11,'Carolyn','carolyn@','1234oi','2010-10-09','1234','1234',6,4,3),(12,'Orozco','orozco@','81dc9bdb52d04dc20036dbd8313ed055','1980-10-09','1234','1234',3,3,3),(13,'Remor','remor@','81dc9bdb52d04dc20036dbd8313ed055','1980-10-09','1234','1234',3,3,3),(14,'Marcus Vinicíus','marcus@sapucaia.edu.ifsul.br','81dc9bdb52d04dc20036dbd8313ed055','1980-10-09','1234','1234',3,3,3),(15,'Adenilson Schimidt','adeni@','81dc9bdb52d04dc20036dbd8313ed055','1980-10-09','1234','1234',3,5,20),(16,'suzan','suzan@gmail.com','81dc9bdb52d04dc20036dbd8313ed055',NULL,'9999999',NULL,0,0,0),(17,'Marines','marines.ps@gmail.com','81dc9bdb52d04dc20036dbd8313ed055',NULL,'51998048194',NULL,0,0,0);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-18 20:51:52
