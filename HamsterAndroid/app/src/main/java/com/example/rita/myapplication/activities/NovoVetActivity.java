package com.example.rita.myapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Veterinario;
import com.example.rita.myapplication.activities.services.VeterinarioService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NovoVetActivity extends AppCompatActivity {
    private EditText etNome, etDescricao, etContato;
    private Button btCadastrar;
    VeterinarioService veterinarioService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_vet);
        this.etContato=(EditText) findViewById(R.id.et_contato);
        this.etNome = (EditText) findViewById(R.id.et_nome);
        this.etDescricao = (EditText) findViewById(R.id.et_descricao);
        this.btCadastrar = (Button) findViewById(R.id.bt_novo_vet);
        veterinarioService = RetrofitUtil.getRetrofit().create(VeterinarioService.class);
        btCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Veterinario veterinario = new Veterinario();
                veterinario.setNome(etNome.getText().toString());
                veterinario.setContato(etContato.getText().toString());
                veterinario.setDescricao(etDescricao.getText().toString());
                veterinarioService.addVeterinarioPost(veterinario).enqueue(new Callback<Veterinario>() {
                    @Override
                    public void onResponse(Call<Veterinario> call, Response<Veterinario> response) {
                        if(response.isSuccessful()){
                            finish();
                        }else{
                            Toast.makeText(NovoVetActivity.this, "Não foi possivel realizar o cadastro", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Veterinario> call, Throwable t) {
                        Toast.makeText(NovoVetActivity.this, "Erro: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }
}
