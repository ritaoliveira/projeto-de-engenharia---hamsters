package com.example.rita.myapplication.activities.Models;

import java.io.Serializable;

public class Veterinario implements Serializable {
    private long idVeterinario;
    private String nome;
    private String descricao;
    private String contato;

    public long getIdVeterinario() {
        return idVeterinario;
    }

    public void setIdVeterinario(long idVeterinario) {
        this.idVeterinario = idVeterinario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    @Override
    public String toString() {
        return nome;
    }
}
