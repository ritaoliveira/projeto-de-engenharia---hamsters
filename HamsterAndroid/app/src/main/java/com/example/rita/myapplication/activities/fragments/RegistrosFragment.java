package com.example.rita.myapplication.activities.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.Models.Vacina;
import com.example.rita.myapplication.activities.NovoRegActivity;
import com.example.rita.myapplication.activities.RetrofitUtil;
import com.example.rita.myapplication.activities.services.HamsterService;
import com.example.rita.myapplication.activities.services.VacinaService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrosFragment extends Fragment {


    private ListView lvVacina;
    VacinaService vacinaService;
    private static Dono dono;
    List<Vacina> vacina;
    private Button btNovoReg;
    public RegistrosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_registros, container, false);
        lvVacina = (ListView)v.findViewById(R.id.lv_reg);
        btNovoReg = (Button)v.findViewById(R.id.bt_reg);
        Bundle b = getArguments();
        if(b!=null){
            dono = (Dono) b.getSerializable("dono");
        }
        vacinaService = RetrofitUtil.getRetrofit().create(VacinaService.class);
        vacinaService.getAllVacinas().enqueue(new Callback<List<Vacina>>() {
            @Override
            public void onResponse(Call<List<Vacina>> call, Response<List<Vacina>> response) {
                if(response.isSuccessful()){
                    vacina = response.body();
                    if(vacina.size()==0){
                        Toast.makeText(getContext(), "Você não adicionou nenhum hamster ainda", Toast.LENGTH_LONG).show();
                    }else {
                        ArrayAdapter<Vacina> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, vacina);
                        lvVacina.setAdapter(adapter);
                    }
                }else{
                    Toast.makeText(getContext(), "Hamsters não encontrados", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<Vacina>> call, Throwable t) {
                Toast.makeText(getContext(), "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        this.btNovoReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itTela = new Intent(getContext(), NovoRegActivity.class);
                itTela.putExtra("dono", dono);
                startActivity(itTela);
            }
        });

        return v;
    }
}
