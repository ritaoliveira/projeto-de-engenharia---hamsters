package com.example.rita.myapplication.activities.Models;

import java.io.Serializable;

public class Vacina implements Serializable {
    private long idVacina;
    private String descricao;
    private String data;
    private String local;
    private Veterinario veterinario;
    private Hamster hamster;

    public long getIdVacina() {
        return idVacina;
    }

    public void setIdVacina(long idVacina) {
        this.idVacina = idVacina;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Veterinario getVeterinario() {
        return veterinario;
    }

    public void setVeterinario(Veterinario veterinario) {
        this.veterinario = veterinario;
    }

    public Hamster getHamster() {
        return hamster;
    }

    public void setHamster(Hamster hamster) {
        this.hamster = hamster;
    }

    @Override
    public String toString() {
        return "Vacina: " + descricao + ", data: " + data + ", realizada em: " + local + ", realizada por: "+veterinario
                + ", em: " + hamster;
    }
}
