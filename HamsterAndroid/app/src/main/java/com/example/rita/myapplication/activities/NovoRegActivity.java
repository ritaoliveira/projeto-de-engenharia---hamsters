package com.example.rita.myapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.Models.Vacina;
import com.example.rita.myapplication.activities.Models.Veterinario;
import com.example.rita.myapplication.activities.services.HamsterService;
import com.example.rita.myapplication.activities.services.VacinaService;
import com.example.rita.myapplication.activities.services.VeterinarioService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NovoRegActivity extends AppCompatActivity {
    private EditText etVacina, data, local;
    private Spinner spHamster, spVet;
    private Button btCriar;
    VeterinarioService veterinarioService;
    HamsterService hamsterService;
    VacinaService vacinaService;
    List<Hamster> hamsters;
    List<Veterinario> veterinarios;
    static Hamster hamster;
    static Veterinario veterinario;
    private Dono dono;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_reg);
        etVacina=(EditText)findViewById(R.id.et_vacina);
        data = (EditText)findViewById(R.id.et_data);
        local = (EditText) findViewById(R.id.et_local);
        spHamster= (Spinner) findViewById(R.id.sp_hamster);
        spVet = (Spinner) findViewById(R.id.sp_vet);
        dono = (Dono)getIntent().getSerializableExtra("dono");
        btCriar=(Button) findViewById(R.id.bt_novo_reg);
        hamsterService = RetrofitUtil.getRetrofit().create(HamsterService.class);
        veterinarioService=RetrofitUtil.getRetrofit().create(VeterinarioService.class);
        vacinaService= RetrofitUtil.getRetrofit().create(VacinaService.class);
        hamsterService.getAllHamsterByDono(dono.getIdUsuario()).enqueue(new Callback<List<Hamster>>() {
            @Override
            public void onResponse(Call<List<Hamster>> call, Response<List<Hamster>> response) {
                if(response.isSuccessful()){
                    hamsters=response.body();
                    ArrayAdapter<Hamster> adapter = new ArrayAdapter<>(NovoRegActivity.this, android.R.layout.simple_list_item_1, hamsters);
                    spHamster.setAdapter(adapter);
                }else{
                    Toast.makeText(NovoRegActivity.this, "Erro em pegar hamsters", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<Hamster>> call, Throwable t) {
                Toast.makeText(NovoRegActivity.this, "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        veterinarioService.getAllVeterinarios().enqueue(new Callback<List<Veterinario>>() {
            @Override
            public void onResponse(Call<List<Veterinario>> call, Response<List<Veterinario>> response) {
                if(response.isSuccessful()){
                    veterinarios=response.body();
                    ArrayAdapter<Veterinario> adapter = new ArrayAdapter<>(NovoRegActivity.this, android.R.layout.simple_list_item_1, veterinarios);
                    spVet.setAdapter(adapter);
                }else{
                    Toast.makeText(NovoRegActivity.this, "Erro em pegar veterinários ", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<Veterinario>> call, Throwable t) {
                Toast.makeText(NovoRegActivity.this, "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
        btCriar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nome = spHamster.getSelectedItem().toString();
                String vet = spVet.getSelectedItem().toString();
                hamsterService.getHamsterByNome(nome).enqueue(new Callback<Hamster>() {
                    @Override
                    public void onResponse(Call<Hamster> call, Response<Hamster> response) {
                        if(response.isSuccessful()){
                            hamster=response.body();
                        }else{
                            Toast.makeText(NovoRegActivity.this, "Erro em pegar hamster ", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<Hamster> call, Throwable t) {
                        Toast.makeText(NovoRegActivity.this, "Erro:  "+t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
                veterinarioService.getVetByNome(vet).enqueue(new Callback<Veterinario>() {
                    @Override
                    public void onResponse(Call<Veterinario> call, Response<Veterinario> response) {
                        if(response.isSuccessful()){
                            veterinario = response.body();
                        }else{
                            Toast.makeText(NovoRegActivity.this, "Erro em pegar veterinario", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<Veterinario> call, Throwable t) {
                        Toast.makeText(NovoRegActivity.this, "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
                Vacina vacina = new Vacina();
                vacina.setDescricao(etVacina.getText().toString());
                vacina.setData(data.getText().toString());
                vacina.setLocal(local.getText().toString());
                vacina.setHamster(hamster);
                vacina.setVeterinario(veterinario);
                vacinaService.addVacinaPost(vacina).enqueue(new Callback<Vacina>() {
                    @Override
                    public void onResponse(Call<Vacina> call, Response<Vacina> response) {
                        if(response.isSuccessful()){
                            Toast.makeText(NovoRegActivity.this, "Registro criado com sucesso", Toast.LENGTH_LONG).show();
                            finish();
                        }else{
                            Toast.makeText(NovoRegActivity.this, "Erro ao cadastrar registro", Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<Vacina> call, Throwable t) {
                        Toast.makeText(NovoRegActivity.this, "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

    }
}
