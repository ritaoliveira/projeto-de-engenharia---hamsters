package com.example.rita.myapplication.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.fragments.HamsterFragment;
import com.example.rita.myapplication.activities.fragments.RegistrosFragment;
import com.example.rita.myapplication.activities.fragments.VeterinariosFragment;

public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FragmentManager fm = getSupportFragmentManager();
    Fragment frag;
    FragmentTransaction ft;
    private static Dono dono;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        dono = (Dono) getIntent().getSerializableExtra("usuario");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        TextView tvNome, tvEmail;
        String text="oi tchau";
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        View view = navigationView.getHeaderView(R.id.nav_header);
        tvNome=findViewById(R.id.nome);
        Resources res = getResources();
        String nome = String.format(res.getString(R.string.nav_header_title), text);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        ft = fm.beginTransaction();

        switch(id){
            case R.id.nav_camera:
                Bundle b= new Bundle();
                b.putSerializable("dono", dono);
                frag = new HamsterFragment();
                frag.setArguments(b);
                ft.replace(R.id.content, frag);
                ft.commit();
                break;
            case R.id.nav_slideshow:
                frag = new VeterinariosFragment();
                ft.replace(R.id.content, frag);
                ft.commit();
                break;
            case R.id.nav_manage:
                Bundle be= new Bundle();
                be.putSerializable("dono", dono);
                frag = new RegistrosFragment();
                frag.setArguments(be);
                ft.replace(R.id.content, frag);
                ft.commit();
                break;
            case R.id.nav_share:
                Intent itSair = new Intent(NavigationActivity.this, LoginActivity.class);
                startActivity(itSair);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
