package com.example.rita.myapplication.activities.services;

import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.Models.Veterinario;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface VeterinarioService {
    @GET("veterinario/all")
    Call<List<Veterinario>> getAllVeterinarios();
    @POST("veterinario")
    Call<Veterinario> addVeterinarioPost(@Body Veterinario veterinario);
    @GET("veterinario/nome/{nome}")
    Call<Veterinario> getVetByNome(@Path("nome")String nome);
    @GET("veterinario/idVeterinario/{idVeterinario}")
    Call<Veterinario> getVetById(@Path("idVeterinario")long idVeterinario);
    @PUT("veterinario/idVeterinario/{idVeterinario}")
    Call<Veterinario> updateVeterinario(@Path("idVeterinario") long idVeterinario, @Body Veterinario veterinario);
}
