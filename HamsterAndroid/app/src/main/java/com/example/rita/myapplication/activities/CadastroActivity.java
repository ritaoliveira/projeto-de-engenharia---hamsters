package com.example.rita.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.services.DonoService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CadastroActivity extends AppCompatActivity {

    private EditText etNome;
    private EditText etEmail;
    private EditText etSenha;
    private EditText etEndereco;
    private Button btCadastrar;
    private DonoService usuarioService;
    private Dono dono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        usuarioService = RetrofitUtil.getRetrofit().create(DonoService.class);
        this.inicializaComponentes();
        this.btCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dono = new Dono();
                dono.setNome(etNome.getText().toString());
                dono.setEmail(etEmail.getText().toString());
                dono.setEndereco(etEndereco.getText().toString());
                dono.setSenha(etSenha.getText().toString());
                usuarioService.addUsuarioPost(dono).enqueue(new Callback<Dono>() {
                    @Override
                    public void onResponse(Call<Dono> call, Response<Dono> response) {
                        if(response.isSuccessful()) {
                            dono=response.body();
                            Toast.makeText(CadastroActivity.this, "Usuário cadastrado com sucesso!", Toast.LENGTH_LONG).show();
                            Intent itTelaMapa= new Intent(CadastroActivity.this, NavigationActivity.class);
                            itTelaMapa.putExtra("usuario", dono);
                            startActivity(itTelaMapa);
                            finish();
                        }else {
                            Toast.makeText(CadastroActivity.this, "Desculpe não foi possível cadastrar o usuário desejado!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Dono> call, Throwable t) {
                        Toast.makeText(CadastroActivity.this, "Desculpe nosso servidor está temporariamente fora do ar! "+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


            }
        });
    }

    private void inicializaComponentes() {
        this.etNome = findViewById(R.id.et_nome);
        this.etEmail = findViewById(R.id.et_email);
        this.etSenha = findViewById(R.id.et_senha);
        this.etEndereco = findViewById(R.id.et_endereço);
        this.btCadastrar = findViewById(R.id.bt_cadastrar);

    }

}
