package com.example.rita.myapplication.activities;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtil {
    public static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl("http://192.168.25.7:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
