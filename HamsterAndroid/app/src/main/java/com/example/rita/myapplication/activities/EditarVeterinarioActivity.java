package com.example.rita.myapplication.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.Models.Veterinario;
import com.example.rita.myapplication.activities.services.VeterinarioService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarVeterinarioActivity extends AppCompatActivity {

    private EditText etNome, etDescricao, etContato;
    private Button btEditar;
    VeterinarioService veterinarioService;
    Veterinario vet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_veterinario);
        this.etContato=(EditText) findViewById(R.id.et_contato_edit);
        this.etNome = (EditText) findViewById(R.id.et_nome_edit);
        this.etDescricao = (EditText) findViewById(R.id.et_descricao_edit);
        this.btEditar = (Button) findViewById(R.id.bt_vet_edit);
        veterinarioService = RetrofitUtil.getRetrofit().create(VeterinarioService.class);
        vet = (Veterinario) getIntent().getSerializableExtra("vet");
        if(vet!=null){
            etNome.setText(vet.getNome());
            etContato.setText(vet.getContato());
            etDescricao.setText(vet.getDescricao());
        }

        btEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vet.setNome(etNome.getText().toString());
                vet.setContato(etContato.getText().toString());
                vet.setDescricao(etDescricao.getText().toString());
                veterinarioService.updateVeterinario(vet.getIdVeterinario(),vet).enqueue(new Callback<Veterinario>() {
                    @Override
                    public void onResponse(Call<Veterinario> call, Response<Veterinario> response) {
                        if(response.isSuccessful()){
                            finish();
                        }else{
                            Toast.makeText(EditarVeterinarioActivity.this, "Não foi possivel realizar a edição", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Veterinario> call, Throwable t) {
                        Toast.makeText(EditarVeterinarioActivity.this, "Erro: "+t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }
}
