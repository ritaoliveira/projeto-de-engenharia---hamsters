package com.example.rita.myapplication.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.services.DonoService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "TelaLoginActivity";
    private static final int ERROR_DIALOG_REQUEST = 9001;
    private EditText etEmail;
    private EditText etSenha;
    private Button btEntrar;
    private TextView tvCadastrar;
    private DonoService nomeService;
    private static Dono dono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        nomeService = RetrofitUtil.getRetrofit().create(DonoService.class);
        this.inicializa();

        this.tvCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent itTelaCad = new Intent(LoginActivity.this,CadastroActivity.class);
                startActivity(itTelaCad);
            }
        });

        this.btEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nomeService.getUserByEmail(etEmail.getText().toString()).enqueue(new Callback<Dono>() {
                                                                                     @Override
                                                                                     public void onResponse(Call<Dono> call, Response<Dono> response) {
                                                                                         if (response.isSuccessful()) {
                                                                                             dono = response.body();
                                                                                             if (etSenha.getText().toString().equals(dono.getSenha())) {
                                                                                                 Toast.makeText(LoginActivity.this, "Bem vindo, " + dono.getNome(), Toast.LENGTH_LONG).show();
                                                                                                 Intent itTelaMapa = new Intent(LoginActivity.this, NavigationActivity.class);
                                                                                                itTelaMapa.putExtra("usuario", dono);
                                                                                                startActivity(itTelaMapa);
                                                                                             } else {
                                                                                                 Toast.makeText(LoginActivity.this, "E-mail ou senha incorretos, tente novamente", Toast.LENGTH_LONG).show();
                                                                                             }
                                                                                         } else {
                                                                                             Toast.makeText(LoginActivity.this, "Usuário inexistente", Toast.LENGTH_LONG).show();
                                                                                         }
                                                                                     }

                                                                                     @Override
                                                                                     public void onFailure(Call<Dono> call, Throwable t) {
                                                                                         Toast.makeText(LoginActivity.this, "Não encontrou usuário : " + t.getMessage(), Toast.LENGTH_LONG).show();
                                                                                     }
                                                                                 }
                );

            }
        });
    }

    private void inicializa() {
        this.etEmail = (EditText) findViewById(R.id.et_mail);
        this.etSenha = (EditText) findViewById(R.id.et_senha);
        this.btEntrar = (Button) findViewById(R.id.bt_entrar);
        this.tvCadastrar = (TextView) findViewById(R.id.tv_cadastrar);


    }



}
