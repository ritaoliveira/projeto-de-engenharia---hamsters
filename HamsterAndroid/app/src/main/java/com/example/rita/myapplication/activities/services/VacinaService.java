package com.example.rita.myapplication.activities.services;

import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.Models.Vacina;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface VacinaService {
    @GET("vacina/all")
    Call<List<Vacina>> getAllVacinas();
    @POST("vacina")
    Call<Vacina> addVacinaPost(@Body Vacina vacina);
    @GET("vacina/idHamster/{idHamster}")
    Call<List<Vacina>> getAllVacinasByHamster(@Path("idHamster") long idHamster);
}
