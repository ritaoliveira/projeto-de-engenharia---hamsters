package com.example.rita.myapplication.activities.services;

import com.example.rita.myapplication.activities.Models.Dono;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DonoService {
    @GET("dono/email/{email}")
    Call<Dono> getUserByEmail(@Path("email") String email);
    @GET("dono/all")
    Call<List<Dono>> getAllUsers();
    @POST("dono")
    Call<Dono> addUsuarioPost(@Body Dono dono);
}
