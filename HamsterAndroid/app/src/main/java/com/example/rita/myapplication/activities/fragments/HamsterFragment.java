package com.example.rita.myapplication.activities.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.NovoHamsterActivity;
import com.example.rita.myapplication.activities.RetrofitUtil;
import com.example.rita.myapplication.activities.services.HamsterService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HamsterFragment extends Fragment {
    private ListView lvHamsters;
    HamsterService hamsterService;
    Dono dono;
    List<Hamster> hamsters;
    private Button btNovo;
    public HamsterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_hamster, container, false);
        lvHamsters = (ListView)v.findViewById(R.id.lv_hamsters);
        Bundle b = getArguments();
        if(b!=null){
            dono = (Dono)getArguments().getSerializable("dono");
        }
        hamsterService = RetrofitUtil.getRetrofit().create(HamsterService.class);
        hamsterService.getAllHamsterByDono(dono.getIdUsuario()).enqueue(new Callback<List<Hamster>>() {
            @Override
            public void onResponse(Call<List<Hamster>> call, Response<List<Hamster>> response) {
                if(response.isSuccessful()){
                    hamsters = response.body();
                    if(hamsters.size()==0){
                        Toast.makeText(getContext(), "Você não adicionou nenhum hamster ainda", Toast.LENGTH_LONG).show();
                    }else {
                        ArrayAdapter<Hamster> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, hamsters);
                        lvHamsters.setAdapter(adapter);
                    }
                }else{
                    Toast.makeText(getContext(), "Hamsters não encontrados", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<Hamster>> call, Throwable t) {
                Toast.makeText(getContext(), "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        btNovo = v.findViewById(R.id.bt_novo);
        this.btNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itTela = new Intent(getContext(), NovoHamsterActivity.class);
                startActivity(itTela);
            }
        });

        return v;
    }

}
