package com.example.rita.myapplication.activities.services;

import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.Models.Vacina;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HamsterService {
    @GET("hamster/all")
    Call<List<Hamster>> getAllHamsters();
    @POST("hamster")
    Call<Hamster> addHamsterPost(@Body Hamster hamster);
    @GET("hamster/idDono/{idDono}")
    Call<List<Hamster>> getAllHamsterByDono(@Path("idDono") long idDono);
    @GET("hamster/nome/{nome}")
    Call<Hamster> getHamsterByNome(@Path("nome") String nome);
}
