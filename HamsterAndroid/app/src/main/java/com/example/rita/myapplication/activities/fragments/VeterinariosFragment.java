package com.example.rita.myapplication.activities.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rita.myapplication.R;
import com.example.rita.myapplication.activities.EditarVeterinarioActivity;
import com.example.rita.myapplication.activities.Models.Dono;
import com.example.rita.myapplication.activities.Models.Hamster;
import com.example.rita.myapplication.activities.Models.Veterinario;
import com.example.rita.myapplication.activities.NovoVetActivity;
import com.example.rita.myapplication.activities.RetrofitUtil;
import com.example.rita.myapplication.activities.services.HamsterService;
import com.example.rita.myapplication.activities.services.VeterinarioService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VeterinariosFragment extends Fragment {

    private ListView lvVet;
    VeterinarioService veterinarioService;
    Dono dono;
    List<Veterinario> veterinarios;
    private Button btNovo;
     static Veterinario veterinario;
    public VeterinariosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_veterinarios, container, false);
        lvVet= (ListView)v.findViewById(R.id.lv_vet);
        btNovo = v.findViewById(R.id.bt_vet);
        veterinarioService = RetrofitUtil.getRetrofit().create(VeterinarioService.class);
        veterinarioService.getAllVeterinarios().enqueue(new Callback<List<Veterinario>>() {
            @Override
            public void onResponse(Call<List<Veterinario>> call, Response<List<Veterinario>> response) {
                if(response.isSuccessful()){
                    veterinarios = response.body();
                    if(veterinarios.size()==0){
                        Toast.makeText(getContext(), "Você não adicionou nenhum hamster ainda", Toast.LENGTH_LONG).show();
                    }else {
                        ArrayAdapter<Veterinario> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, veterinarios);
                        lvVet.setAdapter(adapter);
                        lvVet.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                veterinarioService.getVetById(i+1).enqueue(new Callback<Veterinario>() {
                                    @Override
                                    public void onResponse(Call<Veterinario> call, Response<Veterinario> response) {
                                        if(response.isSuccessful()){
                                            veterinario=response.body();
                                            Intent it = new Intent(getContext(), EditarVeterinarioActivity.class);
                                            it.putExtra("vet", veterinario);
                                            startActivity(it);
                                        }else{
                                            Toast.makeText(getContext(), "Veterinario não encontrado", Toast.LENGTH_LONG).show();

                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Veterinario> call, Throwable t) {
                                        Toast.makeText(getContext(), "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

                                    }
                                });
                            }
                        });
                    }
                }else{
                    Toast.makeText(getContext(), "Hamsters não encontrados", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<Veterinario>> call, Throwable t) {
                Toast.makeText(getContext(), "Erro: "+t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        this.btNovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itTela = new Intent(getContext(), NovoVetActivity.class);
                startActivity(itTela);
            }
        });

        return v;
    }

}
