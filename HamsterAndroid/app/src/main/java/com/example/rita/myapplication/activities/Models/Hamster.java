package com.example.rita.myapplication.activities.Models;

import java.io.Serializable;

public class Hamster implements Serializable {
    private long idHamster;
    private String nome;
    private Dono dono;
    public long getIdHamster() {
        return idHamster;
    }
    public void setIdHamster(long idHamster) {
        this.idHamster = idHamster;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public Dono getDono() {
        return dono;
    }
    public void setDono(Dono dono) {
        this.dono = dono;
    }
    @Override
    public String toString() {
        return  nome ;
    }
}
